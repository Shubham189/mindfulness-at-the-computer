<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el_GR">
<context>
    <name>BreathingDialogComing</name>
    <message>
        <location filename="../../mc/gui/intro_dlg.py" line="327"/>
        <source>Finish</source>
        <translation>Τερματισμός</translation>
    </message>
</context>
<context>
    <name>BreathingNotification</name>
    <message>
        <location filename="../../mc/gui/breathing_notification.py" line="62"/>
        <source>Please slow down and prepare for your breathing break. Please adjust your posture</source>
        <translation>Παρακαλώ χαλαρώστε και προετοιμαστείτε για ασκήσεις αναπνοής. Προσαρμόστε τη στάση σας</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_notification.py" line="79"/>
        <source>Close</source>
        <translation>Κλείσιμο</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_notification.py" line="85"/>
        <source>Show Dialog</source>
        <translation>Εμφάνιση Διαλόγου</translation>
    </message>
</context>
<context>
    <name>BreathingPhraseListWt</name>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="31"/>
        <source>New item</source>
        <translation>Νέα φράση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="40"/>
        <source>Add</source>
        <translation>Προσθήκη</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="49"/>
        <source>Edit the selected breathing phrase</source>
        <translation>Επεξεργασία της επιλεγμένης φράσης αναπνοής</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="55"/>
        <source>Move the selected breathing phrase to top</source>
        <translation>Μετακίνηση επιλεγμένης φράσης αναπνοής στη κορυφή</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="61"/>
        <source>Move the selected breathing phrase up</source>
        <translation>Μετακίνηση επιλεγμένης φράσης αναπνοής προς τα επάνω</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="67"/>
        <source>Move the selected breathing phrase down</source>
        <translation>Μετακίνηση επιλεγμένης φράσης αναπνοής προς τα κάτω</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="73"/>
        <source>Delete the selected breathing phrase</source>
        <translation>Διαγραφή επιλεγμένης φράσης αναπνοής</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="85"/>
        <source>These are the sentences that appear in the `breathing dialog`</source>
        <translation>Προτάσεις που εμφανίζονται στο &apos;Διάλογο Αναπνοής&apos;</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="209"/>
        <source>Cannot remove entry</source>
        <translation>Αδυναμία διαγραφής φράσης</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="209"/>
        <source>You need to have at least one entry in the breathing list</source>
        <translation>Χρειάζεται να υπάρχει τουλάχιστον μία φράση στο διάλογο αναπνοής</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="216"/>
        <source>Are you sure that you want to remove this entry?</source>
        <translation>Είστε σίγουροι ότι επιθυμείτε την διαγραφή της φράσης;</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="232"/>
        <source>You have to write an item before you press &apos;Add&apos;.</source>
        <translation>Πρέπει να εισάγετε μία φράση πριν πατήσετε &apos;Προσθήκη&apos;.</translation>
    </message>
</context>
<context>
    <name>BreathingSettingsWt</name>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="22"/>
        <source>Visual + Audio</source>
        <translation>Οπτική +Ηχητική</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="23"/>
        <source>Visual</source>
        <translation>Οπτική</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="24"/>
        <source>Audio</source>
        <translation>Ηχητική</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="25"/>
        <source>Same</source>
        <translation>Ίδια</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="26"/>
        <source>Random</source>
        <translation>Τυχαία</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="27"/>
        <source>Select audio</source>
        <translation>Επιλογή ήχου</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="67"/>
        <source>Dialog</source>
        <translation>Διάλογος</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="93"/>
        <source>Notifications</source>
        <translation>Ειδοποιήσεις</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="95"/>
        <source>Notification type</source>
        <translation>Τύπος ειδοποίησης</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="100"/>
        <source>Do you always want the same phrase or a random one?</source>
        <translation>Επιθυμείτε πάντα την ίδια φράση ή τυχαία;</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="102"/>
        <source>Dialog Audio</source>
        <translation>Επιλογή Ήχου</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="109"/>
        <source>Settings for Breathing</source>
        <translation>Ρυθμίσεις για αναπνοή</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="198"/>
        <source>Please choose a wav audio file</source>
        <translation>Παρακαλώ επιλέξτε ένα αρχείο ήχου wav</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_settings_wt.py" line="198"/>
        <source>Wav files (*.wav)</source>
        <translation>Αρχεία Wav (*.wav)</translation>
    </message>
</context>
<context>
    <name>EditDialog</name>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="215"/>
        <source>Title</source>
        <translation>Τίτλος</translation>
    </message>
    <message>
        <location filename="../../mc/gui/breathing_phrase_list_wt.py" line="346"/>
        <source>Phrase(s)</source>
        <translation>Φράση(είς)</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="284"/>
        <source>Please choose an image</source>
        <translation>Επιλέξτε μία εικόνα</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="284"/>
        <source>Image files</source>
        <translation>Αρχεία εικόνων</translation>
    </message>
</context>
<context>
    <name>FeedbackDialog</name>
    <message>
        <location filename="../../mc/gui/feedback_dlg.py" line="51"/>
        <source>Show this dialog at startup again in the future</source>
        <translation>Εμφάνιση αυτού του διαλόγου στην εκκίνηση</translation>
    </message>
</context>
<context>
    <name>GeneralSettingsWt</name>
    <message>
        <location filename="../../mc/gui/settings_page_wt.py" line="45"/>
        <source>General Settings</source>
        <translatorcomment>Γενικές ή Βασικές;</translatorcomment>
        <translation>Γενικές Ρυθμίσεις</translation>
    </message>
</context>
<context>
    <name>MainWin</name>
    <message>
        <location filename="../../mc/gui/main_win.py" line="187"/>
        <source>Enable Rest Reminder</source>
        <translation>Ενεργ.Υπενθύμ.Χαλάρωσης</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="198"/>
        <source>Reset Rest Timer (Skip Break)</source>
        <translation type="unfinished">Μηδεν.Χαλάρωσης (Παράλειψη)</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="201"/>
        <source>Take a Break Now</source>
        <translation>Κάντε Διάλειμμα</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="207"/>
        <source>Enable Breathing Reminder</source>
        <translation type="unfinished">Ενερ.Υπενθ.Αναπνοής</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="214"/>
        <source>Open Breathing Dialog</source>
        <translation>Άνοιγμα Διαλόγου Αναπνοής</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="220"/>
        <source>Suspend Application</source>
        <translation>Πρόγραμμα σε αναμονή</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="223"/>
        <source>Open Settings</source>
        <translation>Άνοιγμα Ρυθμίσεων</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="446"/>
        <source>Quit</source>
        <translation>Έξοδος</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="434"/>
        <source>&amp;File</source>
        <translation>&amp;Αρχείο</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="435"/>
        <source>Export data</source>
        <translation>Εξαγωγή δεδομένων</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="438"/>
        <source>Minimize to tray</source>
        <translation type="unfinished">Ελαχιστοποίηση σε εικονίδιο</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="451"/>
        <source>Suspend application</source>
        <translation>Πρόγραμμα σε αναμονή</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="455"/>
        <source>&amp;Debug</source>
        <translation>Απo&amp;σφαλμάτωση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="456"/>
        <source>Update GUI</source>
        <translation>Ενημέρωση Διεπαφής</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="459"/>
        <source>Full screen</source>
        <translation>Πλήρης οθόνη</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="462"/>
        <source>Show rest reminder</source>
        <translatorcomment>ξεκούραση ή χαλάρωση, μήπως να χρησιμοποιήσω συντόμευση</translatorcomment>
        <translation type="unfinished">Εμφάνιση υπενθύμισης χαλάρωσης</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="465"/>
        <source>Show rest prepare</source>
        <translation type="unfinished">Διάλογος Χαλάρωσης</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="469"/>
        <source>&amp;Help</source>
        <translation>&amp;Boήθεια</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="470"/>
        <source>Show intro wizard</source>
        <translation>Εμφάνιση οδηγού έναρξης</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="473"/>
        <source>About</source>
        <translation>Σχετικά</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="476"/>
        <source>Online help</source>
        <translation>Online Βοήθεια</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="479"/>
        <source>Give feedback</source>
        <translation>Ανάδραση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="482"/>
        <source>System Information</source>
        <translatorcomment>μήπως να χρησιμοποιήσω συντόμευση</translatorcomment>
        <translation type="unfinished">Πληροφορίες Συστήματος</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="719"/>
        <source>minute/s left until next rest</source>
        <translatorcomment>λεπτό(-ά), συντομεύσεις</translatorcomment>
        <translation type="unfinished">λεπτό(ά) υπολείπονται για χαλάρωση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/main_win.py" line="727"/>
        <source>Rest breaks disabled</source>
        <translation type="unfinished">Απεν.Διαλειμ.Χαλάρωσης</translation>
    </message>
</context>
<context>
    <name>RestActionListWt</name>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="28"/>
        <source>New item</source>
        <translation>Νέα ενέργεια</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="37"/>
        <source>Add</source>
        <translation>Προσθήκη</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="47"/>
        <source>Edit the selected rest action</source>
        <translation type="unfinished">Επεξεργασία επιλεγμένης ενέργειας για χαλάρωση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="53"/>
        <source>Move the selected rest action to top</source>
        <translation type="unfinished">Μετακίνηση της ενέργειας για χαλάρωση στη κορυφή</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="59"/>
        <source>Move the selected rest action up</source>
        <translation type="unfinished">Μετακίνηση επιλεγμένης ενέργειας για χαλάρωση προς τα πάνω</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="65"/>
        <source>Move the selected rest action down</source>
        <translation type="unfinished">Μετακίνηση επιλεγμένης ενέργειας για χαλάρωση προς τα κάτω</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="71"/>
        <source>Delete the selected rest action</source>
        <translation type="unfinished">Διαγραφή επιλεγμένης ενέργειας για χαλάρωση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="85"/>
        <source>These are the actions that appear in the `rest dialog`</source>
        <translation type="unfinished">Ενέργειες που εμφανίζονται στο &apos;Διάλογο χαλάρωσης&apos;</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="139"/>
        <source>You have to write an item before you press &apos;Add&apos;.</source>
        <translation>Γράψτε μια ενέργεια πριν πατήσετε &apos;Προσθήκη&apos;.</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_action_list_wt.py" line="152"/>
        <source>Are you sure that you want to remove this entry?</source>
        <translation>Είστε σίγουροι ότι επιθυμείτε την διαγραφή της ενέργειας;</translation>
    </message>
</context>
<context>
    <name>RestDlg</name>
    <message>
        <location filename="../../mc/gui/rest_dlg.py" line="32"/>
        <source>Rest Actions</source>
        <translation type="unfinished">Ενέργειες για χαλάρωση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_dlg.py" line="38"/>
        <source>Please move and walk mindfully when leaving the computer</source>
        <translation>Παρακαλώ σηκωθείτε και περπατήστε χαλαρά καθώς απομακρύνεστε από τον υπολογιστή σκεπτόμενοι μόνο το σώμα σας και την αναπνοή σας</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_dlg.py" line="50"/>
        <source>Close</source>
        <translation>Κλείσιμο</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_dlg.py" line="54"/>
        <source>Close and Breathe</source>
        <translation>Κλείσιμο και Αναπνεύστε</translation>
    </message>
</context>
<context>
    <name>RestPrepareDlg</name>
    <message>
        <location filename="../../mc/gui/rest_prepare.py" line="28"/>
        <source>Please prepare for rest</source>
        <translation>Προετοιμαστείτε για χαλάρωση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_prepare.py" line="32"/>
        <source>One minute left until the next rest</source>
        <translation>Ένα λεπτό υπολείπεται για να χαλαρώσετε</translation>
    </message>
</context>
<context>
    <name>RestReminderDlg</name>
    <message>
        <location filename="../../mc/gui/rest_notification.py" line="36"/>
        <source>Please take good care of your body and mind</source>
        <translation>Παρακαλώ φροντίστε το σώμα και το πνεύμα σας</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_notification.py" line="40"/>
        <source>Rest</source>
        <translation>Χαλάρωση</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_notification.py" line="51"/>
        <source>Wait</source>
        <translation>Αναμονή</translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_notification.py" line="55"/>
        <source>Skip</source>
        <translation>Παράλειψη</translation>
    </message>
</context>
<context>
    <name>RestSettingsWt</name>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="23"/>
        <source>Visual + Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="24"/>
        <source>Visual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="80"/>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="27"/>
        <source>Select audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="75"/>
        <source>Notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="77"/>
        <source>Notification type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="88"/>
        <source>Settings for Resting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="128"/>
        <source>Please choose a wav audio file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/rest_settings_wt.py" line="128"/>
        <source>Wav files (*.wav)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RunOnStartupWt</name>
    <message>
        <location filename="../../mc/gui/general_settings_wt.py" line="13"/>
        <source>Run on startup</source>
        <translation>Εκκίνηση στην έναρξη</translation>
    </message>
</context>
<context>
    <name>SettingsPageWt</name>
    <message>
        <location filename="../../mc/gui/settings_page_wt.py" line="22"/>
        <source>Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/settings_page_wt.py" line="23"/>
        <source>Resting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/settings_page_wt.py" line="24"/>
        <source>Timers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mc/gui/settings_page_wt.py" line="26"/>
        <source>General settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SuspendTimeDialog</name>
    <message>
        <location filename="../../mc/gui/suspend_time_dlg.py" line="53"/>
        <source>To resume after having suspended the application, drag the slider to the far left</source>
        <translation>Για επανεκκίνηση της εφαρμογής από αναμονή, σύρετε το χειριστήριο τέρμα αριστερά</translation>
    </message>
    <message>
        <location filename="../../mc/gui/suspend_time_dlg.py" line="78"/>
        <source>Application will resume in normal mode</source>
        <translation>Η εφαρμογή θα συνεχίσει κανονικά</translation>
    </message>
    <message>
        <location filename="../../mc/gui/suspend_time_dlg.py" line="82"/>
        <source>Application will be suspended for</source>
        <translation>Η εφαρμογή θα τεθεί σε αναμονή για</translation>
    </message>
    <message>
        <location filename="../../mc/gui/suspend_time_dlg.py" line="82"/>
        <source>minutes</source>
        <translation>λεπτά</translation>
    </message>
</context>
<context>
    <name>SysinfoDialog</name>
    <message>
        <location filename="../../mc/gui/sysinfo_dlg.py" line="40"/>
        <source>Copy to clipboard</source>
        <translation>Αντιγραφή στο πρόχειρο</translation>
    </message>
</context>
<context>
    <name>TimingInitSetupPage</name>
    <message>
        <location filename="../../mc/gui/intro_dlg.py" line="277"/>
        <source>Rest after minutes</source>
        <translation>Λεπτά για επόμενη χαλάρωση</translation>
    </message>
</context>
<context>
    <name>TimingSettingsWt</name>
    <message>
        <location filename="../../mc/gui/timing_settings_wt.py" line="46"/>
        <source>Reset the rest timer</source>
        <translation type="unfinished">Μηδεν.Χρόν.Χαράλωσης</translation>
    </message>
    <message>
        <location filename="../../mc/gui/timing_settings_wt.py" line="52"/>
        <source>Breathing Dialog</source>
        <translation>Διάλογος Αναπνοής</translation>
    </message>
    <message>
        <location filename="../../mc/gui/timing_settings_wt.py" line="61"/>
        <source>Interval every:</source>
        <translation>Μεσοδιάστημα:</translation>
    </message>
    <message>
        <location filename="../../mc/gui/timing_settings_wt.py" line="63"/>
        <source>minutes</source>
        <translation>λεπτά</translation>
    </message>
    <message>
        <location filename="../../mc/gui/timing_settings_wt.py" line="59"/>
        <source>Rest Dialog</source>
        <translatorcomment>ξεκούρασης ή χαλάρωσης</translatorcomment>
        <translation type="unfinished">Διάλογος χαλάρωσης</translation>
    </message>
    <message>
        <location filename="../../mc/gui/timing_settings_wt.py" line="64"/>
        <source>Time until next break:</source>
        <translation type="unfinished">Διάστημα για χαλάρωση:</translation>
    </message>
    <message>
        <location filename="../../mc/gui/timing_settings_wt.py" line="75"/>
        <source>Settings for Timers</source>
        <translation>Ρυθμίσεις Χρονοδιακοπτών</translation>
    </message>
</context>
<context>
    <name>ToggleSwitchWt</name>
    <message>
        <location filename="../../mc/gui/toggle_switch_wt.py" line="17"/>
        <source>Turn the dialog and notifications on or off</source>
        <translation>Ενεργοποίηση διαλόγου και ειδοποιήσεων</translation>
    </message>
</context>
</TS>
